import sys
import json

def main(args):
    predFile = args[1]
    tacredJsonFile = args[2]
    outputJson = args[3]
    if len(args) > 4:
        hasThreshold = True
        threshold = float(args[4])

    predictions = []

    with open(predFile, "r") as inputFile:
        for line in inputFile:
            predictions.append(line)

    tacredJson = []

    with open(tacredJsonFile, "r") as inputFile:
        tacredJson = json.load(inputFile)

    if(len(predictions) != len(tacredJson)):
        raise Exception("file sizes are not compatible")

    output = []
    for i in range(len(predictions)):
        example = tacredJson[i]
        if(hasThreshold):
            score = float(predictions[i])
            if score > threshold:
                example["luke_annotation"] = "relation"
            else:
                example["luke_annotation"] = "no_relation"
        else :
            example["luke_annotation"] = predictions[i]
        output.append(example)

    with open(outputJson, "w") as outputFile:
        json.dump(output, outputFile)


if __name__ == "__main__":
    main(sys.argv)

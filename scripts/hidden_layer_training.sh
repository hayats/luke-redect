#!/bin/bash

module load pytorch/1.5.1-py3.7 
export TMPDIR=/srv/tempdd/hayats/cache
mkdir "/srv/tempdd/hayats/LUKE/out-sigmo-$1"
cd "/srv/tempdd/hayats/LUKE/luke-redect"
/srv/tempdd/hayats/cache/virtualenvs/luke-o4rjaoDY-py3.7/bin/python -m examples.cli \\
	"--model-file=/srv/tempdd/hayats/LUKE/model/luke_large_500k.tar.gz"\\
       	"--output-dir=/srv/tempdd/hayats/LUKE/out-sigmo-$1"\\
       	relation-detection run\\
       	"--data-dir=/srv/tempdd/hayats/LUKE/tacred"\\
       	"--train-batch-size=4"\\
       	"--gradient-accumulation-steps=8"\\
       	"--learning-rate=1e-5"\\
       	"--no-eval"\\
	"--num-train-epochs=5"\\
       	"--fp16"\\
       	"--hidden-layer-size=$1"

